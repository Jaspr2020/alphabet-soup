class vec2:
    def __init__(self, x, y):
        self.x = x
        self.y = y
        
    def __sub__(self, other):
        return vec2(self.x - other.x, self.y - other.y)

    def __add__(self, other):
        return vec2(self.x + other.x, self.y + other.y)
    
    def __mul__(self, other: int):
        return vec2(self.x * other, self.y * other)
    
    def __repr__(self):
        return f"{self.x}:{self.y}"
    
    def __str__(self):
        return f"{self.x}:{self.y}"
    
def argument_parsing() -> str:
    """Returns path to input file if valid, otherwise exits program"""
    import sys
    import os.path
    
    # No argumetns
    if len(sys.argv) == 1:
        print("Please run with an additional argument which is the relative path to a puzzle text file")
        
    # One argument
    elif len(sys.argv) == 2:
        # Argument is a file: return argument
        if os.path.isfile(sys.argv[1]):
            return sys.argv[1]
        
        # Argument is not a file
        else:
            print("File does not exist")
            
    # Too many arguments
    else:
        print("Too many arguments")

    exit()    

def adjacent_letter_positions(puzzle: list, letter: str, position: vec2) -> list:
    """Checks if the passed letter is in any squares adjacent to the passed position
    \nOutput is a list of vec2s of all positions of the passed letter"""
    positions = []
    
    # Loop through adjacent squares
    for i in range(-1, 2):
        # Avoid horrizontal off-grid indices
        if not 0 <= position.x + i < len(puzzle):
            continue
        
        for j in range(-1, 2):
            # Avoid vertical off-grid indices (and center square)
            if not 0 <= position.y + j < len(puzzle[0]) or (i == 0 and j == 0):
                continue
            
            # Calculate new position
            new_position = position + vec2(i, j)
            
            # If the letter is in that position, add the position to the list
            if puzzle[new_position.x][new_position.y] is letter:
                positions.append(new_position)

    return positions

def check_solution(puzzle: list, word: str, start: vec2, direction: vec2) -> bool:
    """Returns whether or not a potential solution is correct"""
    end_position = start + direction * (len(word) - 1)
    
    # If the solution fits in the puzzle
    if 0 <= end_position.x < len(puzzle) and 0 <= end_position.y < len(puzzle[0]):
        # Get all letters from the potential solution
        letters = [puzzle[start.x + i * direction.x][start.y + i * direction.y] for i in range(len(word))]
        # Return whether or not the solution is correct
        return word == "".join(letters)
    
    # Solution does not fit in puzzle
    return False

def solve_puzzle(puzzle: list, words_to_find: list) -> dict:
    """Returns a dictionary with pairs of words and solution locations
    \n{ word : (start_position, end_position) }"""
    answers = {}
    
    # Remove spaces from the words
    words = [word.replace(' ', '') for word in words_to_find]
    
    # For each word in the list
    for index, word in enumerate(words):
        # Store all positions of the first letter
        start_positions = []
        [[start_positions.append(vec2(row_num, col_num)) for col_num, letter in enumerate(row) if letter is word[0]] for row_num, row in enumerate(puzzle)]
        
        for start_position in start_positions:
            # Stop checking positions if the answer was found
            if words_to_find[index] in answers:
                break
            
            # Find directions of possible solutions (based on second letter)
            word_directions = [(second_position - start_position) for second_position in adjacent_letter_positions(puzzle, word[1], start_position)]
            
            for direction in word_directions:
                # If the start position + direction are the solution, add it to the dictionary and end the loop 
                if check_solution(puzzle, word, start_position, direction):
                    answers[words_to_find[index]] = (start_position, start_position + direction * (len(word) - 1))
                    break

    return answers   

def main():
    input_file = argument_parsing()
    
    # Read text from input file
    with open(input_file) as f:
        lines = f.readlines()
    f.close()
    
    # Remove newline character from text
    lines = [line.replace('\n', '') for line in lines]
    
    puzzle_size = [int(value) for value in lines[0].split('x')]
    
    # Populate puzzle from file
    puzzle = [['' for i in range(puzzle_size[0])] for j in range(puzzle_size[1])]
    for i in range(1, 1 + puzzle_size[1]):
        puzzle[i - 1] = lines[i].split(' ')
        
    words_to_find = lines[1 + puzzle_size[1]:]
    
    # Get puzzle solutions
    solutions = solve_puzzle(puzzle, words_to_find)

    # Save solution to a text file in the ./answers/ folder
    f = open("./answers/" + input_file.split('/')[-1].split('.txt')[0] + "_solution.txt", 'w')
    for word in words_to_find:
        solution = solutions[word]
        f.write(word + ' ' + str(solution[0]) + ' ' + str(solution[1]) + '\n')
    f.close()
        
if __name__ == "__main__":
    main()